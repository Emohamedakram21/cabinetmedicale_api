const express = require('express');
const MutulleRouter = express.Router();
const myChaines = require('../chaines/strQuery');
const myPool = require('../config/database');
const auth = require('../middleware/auth');

//ALL
MutulleRouter.post('/All',auth,async(req,res)=>{
    try{
        const {id=0,libelle=""} = req.body;
        const mutuelle = await myPool.query(myChaines.all_Mutuelle,[id,libelle]);
        res.json(mutuelle.rows);
    }catch(error)
    {
        res.json(error.message);
    }
});
//TARITER
MutulleRouter.post('/traiter',auth,async(req,res)=>{
    try{
        const {id,libelle,telephone} = req.body;       
        if( !libelle || !telephone )
        {
            return res.status(400).send("Désolé, veuillez renseigner les élements obligatoires ");
        }
        const indexMutuelle = await myPool.query(myChaines.all_Mutuelle,[0,libelle]);
        if(indexMutuelle.rowCount>0)
        {
            return res.status(409).send("Exist déjà : "+libelle);       
        }
        if(id == 0){
            await myPool.query(myChaines.insert_Mutuelle,[id,libelle,telephone]);
        }
        else{
            await myPool.query(myChaines.update_Mutuelle,[id,libelle,telephone]);
        }
        const mutuelleAll = await myPool.query(myChaines.all_Mutuelle,[0,'']);
        res.json(mutuelleAll.rows);
    }catch(error){  
        res.json(error.message);
    }
});
//DELETE
MutulleRouter.post('/delete',auth,async(req,res)=>{
    try{
        const {id} = req.body;
        await myPool.query(myChaines.delete_Mutuelle,[id]);
        const mutuelleAll = await myPool.query(myChaines.all_Mutuelle,[0,'']);
        res.json(mutuelleAll.rows);
    }catch(error)
    {
        res.json(error.message);
    }
});

module.exports = MutulleRouter;



