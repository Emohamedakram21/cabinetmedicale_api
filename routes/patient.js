const express = require('express');
const PatientRouter = express.Router();
const myChaines = require('../chaines/strQuery');
const myPool = require('../config/database');
const auth = require('../middleware/auth');

//ALL
PatientRouter.post('/All',auth,async(req,res)=>{
    try{
        const {id=0,name="",cin=""} = req.body;
        const patient = await myPool.query(myChaines.All_Patient,[id,cin,name]);
        res.json(patient.rows);
    }catch(error)
    {
        res.status(401).send(error.message);
    }
});
//TARITER
PatientRouter.post('/traiter',auth,async(req,res)=>{
    try{
        const {id,cin,nomcomplet,datenaissance,sexe,adresse,telephone,email,situation,fk_mutuelle_id,fk_users_id} = req.body;       
        if( !cin || !nomcomplet || !datenaissance || !sexe || !telephone  )
        {
            return res.status(400).send("Désolé, veuillez renseigner les élements obligatoires ");
        }
       
        if(id == 0){
           /* const UniquePatient = await myPool.query(myChaines.All_Patient,[0,cin,nomcomplet]);
            if(UniquePatient.rowCount>0)
            {
                return res.status(409).send("Exist déjà : "+nomcomplet);       
            }else{*/
                await myPool.query(myChaines.insert_Patient,[id,cin,nomcomplet,datenaissance,sexe,adresse,telephone,email,situation,fk_mutuelle_id]);
           // }
        }
        else{
            await myPool.query(myChaines.update_Patient,[id,cin,nomcomplet,datenaissance,sexe,adresse,telephone,email,situation,fk_mutuelle_id]);
        }
        const patientAll = await myPool.query(myChaines.All_Patient,[0,'','']);
        res.json(patientAll.rows);
    }catch(error){  
        res.status(401).send(error.message);
    }
});
//DELETE
PatientRouter.post('/delete',auth,async(req,res)=>{
    try{
        const {id} = req.body;
        await myPool.query(myChaines.delete_patient,[id]);
        const patientAll = await myPool.query(myChaines.All_Patient,[0,'','']);
        res.json(patientAll.rows);
    }catch(error)
    {
        res.status(401).send(error.message);
    }
});



module.exports = PatientRouter;


