require("dotenv").config();
const express = require("express");
const usersRouter = express.Router();
const myChaines = require('../chaines/strUsers');
const myPool = require('../config/database');
const auth = require("../middleware/auth");
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken");



//ALL
usersRouter.post('/All',auth,async(req,res)=>{
    try{
        const {id=0,name=""} = req.body;
        const users = await myPool.query(myChaines.all,[id,name]);
        res.json(users.rows);
    }catch(error)
    {
        res.status(401).send(error.message);
    }
});
//Tariter
usersRouter.post('/register',async(req,res)=>{
    try{
        const {id,nom,pword,email} = req.body;            
        if(!(nom  || pword || email)){
          return  res.status(400).send("Désolé, veuillez renseigner les éléments obligatoires");
        }

        if(id == 0)
        {
           /* const oldUser = await myPool.query(myChaines.byLogin,[email]);
            if(oldUser.rowCount > 0){
                return res.status(409).send("Désolé, ce compte existe déjà");
            }*/

            const encryptedPassword = await bcrypt.hash(pword,10);
            await myPool.query(myChaines.insert,[nom,email,encryptedPassword]);
        }
        else{

            if(pword)
            {
                const encryptedPassword = await bcrypt.hash(pword,10);
            await myPool.query(myChaines.update,[id,nom,email,encryptedPassword]);
            }
            else{
                await myPool.query(myChaines.update,[id,nom,email,'']);
            }
        }

        const userID = await myPool.query(myChaines.byId,[0]);
        res.json(userID.rows);
      
    }catch(error){
        res.status(401).send(error.message);
    };
})




usersRouter.post("/login",async (req,res)=>{
    try{
        const {login , pword} = req.body;

        if(!(login && pword)){
           return  res.status(400).send("Désolé, veuillez renseigner le login et le mot de passe");
        }

        const user = await myPool.query(myChaines.byLogin,[login]);

        if(user && (await bcrypt.compare(pword,user.rows[0].pword))){
            const _id = user.rows[0].id;
            const myToken = jwt.sign({user_Id: _id,login},process.env.TOKEN_KEY,{expiresIn: "8h"});
            await myPool.query(myChaines.accToken,[_id,myToken]);
            res.status(200).json(user.rows);            
        }else{
            res.status(401).send("Désolé, login ou mot de passe incorrects");
        }  
    }
    catch(error){
        res.status(401).send(error.message);
    }
});

//DELETE
usersRouter.post('/delete',auth,async(req,res)=>{
    try{
        const {id} = req.body;
        await myPool.query(myChaines.delete,[id]);
         const users = await myPool.query(myChaines.all,[0,'']);
        res.json(users.rows);
    }catch(error)
    {
        res.status(401).send(error.message);
    }
});


module.exports = usersRouter;


