require('dotenv').config();

const myHttp = require("http");
const myApp = require("./app");

const myServer = myHttp.createServer(myApp);

const port = process.env.API_PORT;

myServer.listen(port,()=>{
    console.log(`J'écoute sur le port ${port}`);
})

