require("dotenv").config();

const myExpress = require("express");

const collabs = require("./routes/user");
const mutuelle = require('./routes/mutuelle')
const patient = require('./routes/patient')
const cors = require('cors');

const myApp = myExpress();

myApp.use(cors());
myApp.use(myExpress.json({limit:'50mb'}));
//Routes
myApp.use('/collab',collabs);
myApp.use('/mutuelle',mutuelle);
myApp.use('/patient',patient);

module.exports = myApp;

