const qryUsers = {
    all: "select * from users.fn_users($1,$2)",
    byId: "select * from users.fn_users($1,'')",
    byLogin: "select * from users.fn_usersByLogin($1)",
    insert : "select users.ps_user_cr(0,$1,$2,$3)",
    update : "select users.ps_users_u($1,$2,$3,$4)",
    
    accToken :"select users.ps_users_acc($1,$2)",    
    changePassword : "select * from users.ps_changePassword($1,$2)",
    delete : "select users.ps_users_d($1,'D')"
};

module.exports = qryUsers;

