const query = {
    all_Mutuelle:"select * from fn_mutuelle($1,$2)",
    insert_Mutuelle : "select  public.ps_Mutuelle_cr($1,$2,$3)",
    update_Mutuelle : "select  public.ps_Mutuelle_u($1,$2,$3)",
    delete_Mutuelle : "select  public.ps_Mutuelle_d($1,'D')",
    //Patient
    All_Patient:"select * from public.fn_patient($1,$2,$3)",
    insert_Patient : "SELECT public.ps_patient_cr($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)",
    update_Patient : "SELECT public.ps_patient_u($1,$2,$3,$4,$5,$6,$7,$8,$9,$10)",
    delete_patient : "select  public.ps_patient_d($1, 'D')",   
};


module.exports = query;



