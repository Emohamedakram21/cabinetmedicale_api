require('dotenv').config();
const Pool = require('pg').Pool;

const myPool = new Pool({
    host : process.env.DB_HOST,
    database: process.env.DB_DATABASE,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    max: process.env.DB_MAX_CNX,

})

module.exports = myPool;